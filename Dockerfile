FROM python:3.8-bullseye
WORKDIR /app
COPY requirements.txt ./
RUN pip install --no-cache-dir Flask==2.0.1 Werkzeug==2.2.2
EXPOSE 4000
COPY . /app
CMD ["python", "app.py"]
